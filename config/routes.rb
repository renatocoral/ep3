Rails.application.routes.draw do
  devise_for :users
  resources :cars
  resources :alunos
  get 'demo/index'
  get 'demo/listagem'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

class CreatePessoas < ActiveRecord::Migration[5.2]
  def change
    create_table :pessoas do |t|
      t.string :nome
      t.string :email
      t.integer :idade

      t.timestamps
    end
    add_index :pessoas, :email, unique: true
  end
end

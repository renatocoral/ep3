class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :modelo
      t.integer :ano
      t.string :cor

      t.timestamps
    end
  end
end

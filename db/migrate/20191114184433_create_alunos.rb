class CreateAlunos < ActiveRecord::Migration[5.2]
  def change
    create_table :alunos do |t|
      t.string :nome
      t.string :telefone
      t.integer :matricula
      t.float :ira

      t.timestamps
    end
    add_index :alunos, :matricula, unique: true
  end
end

require 'test_helper'

class DemoControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get demo_index_url
    assert_response :success
  end

  test "should get listagem" do
    get demo_listagem_url
    assert_response :success
  end

end

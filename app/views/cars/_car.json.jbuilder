json.extract! car, :id, :modelo, :ano, :cor, :created_at, :updated_at
json.url car_url(car, format: :json)
